package com.dong.spring.controllers;

import com.dong.spring.dto.consumes.StudentConsumeDto;
import com.dong.spring.dto.produces.StudentProduceDto;
import com.dong.spring.services.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${base.api}/student")
@RequiredArgsConstructor
public class StudentController {

    private  final StudentService mStudentService;
    @PostMapping
    public ResponseEntity<StudentProduceDto> createStudent(@RequestBody StudentConsumeDto studentConsumeDto){
        return  ResponseEntity.status(HttpStatus.CREATED).body(mStudentService.createStudent(studentConsumeDto));
    }
    @GetMapping
    public ResponseEntity<List<StudentProduceDto>> getAllStudent(){
            return  ResponseEntity.status(HttpStatus.OK).body(mStudentService.getAllStudent());
    }
    @GetMapping("/{id}")
    public ResponseEntity<StudentProduceDto> getByid(@PathVariable Long id) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(mStudentService.getByID(id));
    }
    @PutMapping("{id}")
    public ResponseEntity<StudentProduceDto> editByID(@PathVariable Long id,@RequestBody StudentConsumeDto studentConsumeDto){
        return ResponseEntity.ok().body(mStudentService.editById(id,studentConsumeDto));
    }
    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteByID(@PathVariable Long id){
        mStudentService.deleteByID(id);
        return ResponseEntity.ok().body("Xoa thanh cong");
    }

}
