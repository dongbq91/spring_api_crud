package com.dong.spring.repository;

import com.dong.spring.entities.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository  extends JpaRepository<StudentEntity, Long> {
    StudentEntity findByName(String s);
//    @Query(nativeQuery = true , value = "select * from student where name =?1")
//    StudentEntity findByNameQuery(String s);
}
