package com.dong.spring.services;

import com.dong.spring.dto.consumes.StudentConsumeDto;
import com.dong.spring.dto.produces.StudentProduceDto;
import com.dong.spring.entities.StudentEntity;
import com.dong.spring.exceptions.BadRequestException;
import com.dong.spring.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class StudentService {
    private final StudentRepository mStudentRepository;

    public StudentProduceDto createStudent(StudentConsumeDto studentConsumeDto){
        StudentEntity studentEntity = studentConsumeDto.toStudentEntity();
        StudentEntity studentEntity1 = mStudentRepository.findByName(studentEntity.getName());
        if (studentEntity1 != null){
            throw new BadRequestException("Da ton tai");
        }
        return new StudentProduceDto().toStudentProduceDto(mStudentRepository.save(studentEntity));
    }
    public List<StudentProduceDto> getAllStudent(){
        List<StudentEntity> studentEntityList = mStudentRepository.findAll();
        return studentEntityList.stream().map(o->
                new StudentProduceDto().toStudentProduceDto(o)).collect(Collectors.toList());
    }
    public StudentProduceDto getByID(Long id) throws Exception {
        StudentEntity  studentEntity = mStudentRepository.findById(id).orElse(null);
        if(studentEntity == null){
            throw new BadRequestException("ID khong ton tai");
        }
        else {
            return new StudentProduceDto().toStudentProduceDto(studentEntity);
        }
    }
    public  StudentProduceDto editById( Long id, StudentConsumeDto studentConsumeDto){
        StudentEntity studentEntity = mStudentRepository.findById(id).orElse(null);
        if(Objects.isNull(studentEntity)){
            throw  new BadRequestException(("ID khong ton tai"));
        }

            studentEntity.setName(studentConsumeDto.getName());
            mStudentRepository.save(studentEntity);
            return  new StudentProduceDto().toStudentProduceDto(studentEntity);
    }
    public void  deleteByID(Long id){
        StudentEntity studentEntity = mStudentRepository.findById(id).orElse(null);
        if(Objects.isNull(studentEntity)){
            throw  new BadRequestException(("ID khong ton tai"));
        }
        mStudentRepository.deleteById(id);
    }
}
