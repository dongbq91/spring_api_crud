package com.dong.spring.exceptions;


public class BadRequestException extends RuntimeException{

    public  BadRequestException(String massage){
        super(massage);
    }
}
