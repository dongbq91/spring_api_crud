package com.dong.spring.dto.produces;

import com.dong.spring.entities.StudentEntity;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentProduceDto {
    private Long id;
    private  String name;
    public  StudentProduceDto toStudentProduceDto(StudentEntity studentEntity){
        return  StudentProduceDto.builder()
                .id(studentEntity.getId())
                .name(studentEntity.getName())
                .build();
    }
}
