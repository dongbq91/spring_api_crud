package com.dong.spring.dto.produces;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ExceptionProduceDto {
    private String massage;
    private Integer code;
}
