package com.dong.spring.dto.consumes;

import com.dong.spring.entities.StudentEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StudentConsumeDto {
    private String name;

    public StudentEntity toStudentEntity(){
        return  StudentEntity.builder()
                .name(name)
                .build();
    }
}
